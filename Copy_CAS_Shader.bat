@echo off
IF EXIST %1%\Engine\Shaders\Private\NUL (
	xcopy /s Shaders\*.ush %1%\Engine\Shaders\Private\
	echo Finished copying CAS shaders
) ELSE (
	echo Invalid Directory: %1%
	echo Please provide a valid Unreal Engine repository director.
)